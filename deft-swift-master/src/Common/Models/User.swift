//
//  User.swift
//  DEFT
//
//  Created by Rupendra on 21/08/20.
//  Copyright © 2020 Example. All rights reserved.
//

import UIKit

class User: NSObject {
    var firebaseUserId :String?
    var emailAddress :String?
    var password :String?
    var lockPassword :String?
}
